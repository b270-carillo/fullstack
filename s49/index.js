// console.log("Friday Night")

// fetch()
	// this is a method in JS that is used to send requests to the server and load responses from the server in webpages

// Get post data
fetch("https://jsonplaceholder.typicode.com/posts")
.then((response) => response.json())
.then((data) => showPosts(data));

// Show Post
const showPosts = (posts) => {

	let postEntries = "";

	posts.forEach((post) => {
		console.log(posts);

		// We can assign HTML elements in JS variables
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}"> ${post.title} </h3>
				<p id="post-body-${post.id}"> ${post.body} </p>
				<button onclick="editPost(${post.id})"> Edit </button>
				<button onclick="deletePost(${post.id})"> Delete </button>
			</div>
		`;
	});

	document.querySelector("#div-post-entries").innerHTML = postEntries;
}

// Add a post
// We select the Add form using the querySelector
// We listen with the submit button for events

document.querySelector("#form-add-post").addEventListener("submit", (e) => {

    e.preventDefault();

    fetch("https://jsonplaceholder.typicode.com/posts", {
        method: "POST",
        headers: {"Content-type": "application/json"},
        body: JSON.stringify({
            title: document.querySelector("#txt-title").value,
            body: document.querySelector("#txt-body").value,
            userId: 1
        })
    })

    .then((response) => response.json())
    .then((data) => {
        console.log(data);
        alert("Successfully added!");

	    // Clear input fields or resets th state of our input blanks after submitting a new post
		document.querySelector("#txt-title").value = null;
		document.querySelector("#txt-body").value = null;
    });
});


// Update post

document.querySelector("#form-edit-post").addEventListener("submit", (e) => {

    e.preventDefault();

    fetch("https://jsonplaceholder.typicode.com/posts/1", {
    	method: "PUT",
    	headers: {"Content-type": "application/json"},
    	body: JSON.stringify({
    		id: document.querySelector("#txt-edit-id").value,
    		title: document.querySelector("#txt-edit-title").value,
    		body: document.querySelector("#txt-edit-body").value,
    		userId: 1
    	})
    })
    .then((response) => response.json())
    .then((data) => {
    	console.log(data);
    	alert("Successfully updated!");

    	document.querySelector("#txt-edit-id").value = null;
    	document.querySelector("#txt-edit-title").value = null;
    	document.querySelector("#txt-edit-body").value = null;
    	document.querySelector("#btn-submit-update").setAttribute("disabled", true);
    });
});

// Edit post
const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;
	document.querySelector("#btn-submit-update").removeAttribute("disabled");

}


//Activity Delete

const deletePost = (id) => {
  
  fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
    method: "DELETE",
  })
    .then((response) => {
      if (response.ok) {        
        document.querySelector(`#post-${id}`).remove();
        alert(`Post successfully deleted! ${id}`);
      } else {
        console.error(`Failed to delete post with id ${id}.`);
      }
    })
    .catch((error) => {
      console.error(`Failed to delete post with id ${id}.`, error);
    });
};


















